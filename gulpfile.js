var gulp = require('gulp'),
	less = require('gulp-less'),
	autoprefixer = require('gulp-autoprefixer'),
	pug = require('gulp-pug'),
	livereload = require('gulp-livereload');
	
//
// gulp.task('less', function () {
//   return gulp.src('./less/*.less')
//     .pipe(less())
// 	.pipe(autoprefixer({
// 	    browsers: ['last 15 versions'],
// 	    cascade: false
// 	}))
//     .pipe(gulp.dest('./css'))
//     .pipe(livereload());
// });

function styles() {
	return gulp.src('./less/*.less')
		.pipe(less())
		.pipe(autoprefixer({
			browsers: ['last 15 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./css'))
		.pipe(livereload());
}

function templates (){
	return gulp.src(['./*.pug', './pug-templates/*.pug'])
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('./'))
		.pipe(livereload());
}

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch(['./*.pug', './pug-templates/*.pug'], templates);
	gulp.watch('./less/*.less', styles);
});