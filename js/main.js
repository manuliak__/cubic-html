$(document).ready(function () {

    new WOW().init({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false,
        live: true
    });

    resizeFont();
    homeProductsSlider();
    searchFormFocus();
    footerAccordion();

    $('#reviewsSlider').owlCarousel({
        items: 1,
        nav: true,
        navText: ["<span class='icon-angle-left'></span>", "<span class='icon-angle-right'></span>"]
    });

    $('#premiumProducts').owlCarousel({
        items: 4,
        nav: true,
        navText: ["<span class='icon-angle-left'></span>", "<span class='icon-angle-right'></span>"],
        responsive: {
            0: {
                items: 1
            },
            420: {
                items: 2
            },
            768: {
                items: 4
            },
        }
    });

    $(".scroll-down-button a").click(function (e) {
        e.preventDefault();

        $('html, body').animate(
            {scrollTop: $($(this).attr('href')).offset().top},
            1000
        );

    });

    $( window ).resize(function() {
        resizeFont();
    });

    $("#mmenu").mmenu({
        extensions: ["position-bottom", "fullscreen", "fx-panels-left", "fx-listitems-drop", "border-offset"],
        "navbars": [
            {
                "content": [
                    "<span class='mmenu-page-logo'><img src='" + $('.header-logo__img--mobile').attr('src') + "'></span>",
                    "prev",
                    "title",
                    "close"
                ]
            }
        ],
        hooks: {
            "openPanel:start": function ($panel) {
                if ($panel.attr("id") == 'panel-menu') {
                    $('.mm-navbar__title').addClass('hiden-status');
                    $('.mmenu-page-logo').removeClass('hiden-status');
                } else {
                    $('.mm-navbar__title').removeClass('hiden-status');
                    $('.mmenu-page-logo').addClass('hiden-status');
                }
            }
        }
    });

    rating();
    closeRm();
    moreContent();

});

/*****************************************************
 *
 *
 *          Update Font Size
 *
 *
 * ****************************************************/

function resizeFont() {
    var window_width = $(window).width();
    var control_width = 1920;
    var font_size = 16;
    var new_font_size = ((window_width * font_size) / control_width).toFixed(1);
    new_font_size += 'px';

    if ($(document).width() > 768) {
        $('html, body').css({'font-size': new_font_size});
    } else {
        $('html, body').css({'font-size': font_size + 'px'});
    }

    var all_images = $('img');
    all_images.each(function () {
        var img_width = $(this).attr('width');
        var img_em = 0;
        if (img_width != '') {
            if (!isNaN(img_width)) {
                img_em = img_width / font_size;
                img_em = img_em + 'rem';
                $(this).css({'width': img_em});
            }
        }
    });

}

/*****************************************************
 *
 *
 *          Home products slider
 *
 *
 * ****************************************************/

function homeProductsSlider() {

    var slider = $('#homeProductsSlider'),
        sliderWrapper = slider.closest('.top-slider__container'),
        sliderItems = $('.products-slider__container', slider).children(),
        sliderControls = '',
        sliderInterval,
        animations = "animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd";

    // Creating a nav of slider and make active a first slide
    if (sliderItems.length > 0) {

        sliderItems.each(function (index, slide) {

            var slideNumber = (sliderItems.length > 10) ? index : '0' + (index + 1),
                pageStatus = '';

            if (index == 0) {

                $(slide).addClass('active_slide');
                pageStatus = " active";

                $('img', slide).addClass('animated slideInRight').one(animations, function () {

                    $(this).toggleClass('animated slideInRight');

                });

            }

            sliderControls += "<span class='slider-page" + pageStatus + "'>" + slideNumber + "</span>"

        });

        if (sliderItems.length > 1)
            sliderWrapper.append("<div class='slider-controls'>" + sliderControls + "</div>");

        startAutoplay();
    }


    // Click Event on the slider
    sliderWrapper.find('.slider-page').on('click', function () {

        clearInterval(sliderInterval);

        if (!$(this).hasClass('active')) {

            var currentPageIndex = $('.slider-page.active').index(),
                newPageIndex = $(this).index(),
                currentPage = sliderItems[currentPageIndex],
                newPage = sliderItems[newPageIndex];

            $(currentPage).removeClass('active_slide');
            $(newPage).addClass('active_slide');

            $('.slider-page').eq(currentPageIndex).removeClass('active');
            $(this).addClass('active');

            $('img', newPage).addClass('animated slideInRight').one(animations, function () {

                $(this).toggleClass('animated slideInRight');

            });
        }
        startAutoplay();
    });

    // an autoplay functions of the slider
    function sliderAutoplay() {

        if (sliderItems.length > 1) {

            var currentPageIndex = sliderWrapper.find('.slider-page.active').index(),
                newPageIndex = (currentPageIndex + 1 == sliderItems.length) ? $('.slider-page:first').index() : currentPageIndex + 1,
                currentPage = sliderItems[currentPageIndex],
                newPage = sliderItems[newPageIndex];

            $(currentPage).removeClass('active_slide');
            $(newPage).addClass('active_slide');

            $('.slider-page').eq(currentPageIndex).removeClass('active');
            $('.slider-page').eq(newPageIndex).addClass('active');

            $('img', newPage).addClass('animated slideInRight').one(animations, function () {

                $(this).toggleClass('animated slideInRight');

            });
        }

    }

    function stopAutoplay() {
        clearInterval(sliderInterval);
    }

    function startAutoplay() {
        sliderInterval = setInterval(sliderAutoplay, 4000);
    }

}

/*****************************************************
 *
 *
 *          Search form
 *
 *
 * ****************************************************/

function searchFormFocus() {
    $('.header-search input').focusin(function () {

        $(this).closest('.search-form__content').addClass('header-search--focus_on');

    });
    $('.header-search input').focusout(function () {

        $(this).closest('.search-form__content').removeClass('header-search--focus_on');

    });
}

/*****************************************************
 *
 *
 *          Footer mobile accordion
 *
 *
 * ****************************************************/

function footerAccordion() {

    var acc = document.getElementsByClassName("footer-menu__title");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {

            if ($(document).width() < 768) {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                panel.classList.toggle("isOpen");
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            }
        });
    }

}

/*****************************************************
 *
 *
 *          Set rating
 *
 *
 * ****************************************************/

function rating() {

    var ratingStar = $('.rating-label');

    ratingStar.click(function () {
        var ratingElIndex = $(this).closest('.rating-field').index();

        $('.rating-field').removeClass('active-rating');
        for (var ind = 0; ind <= ratingElIndex; ind++) {
            $('.rating-field').eq(ind).addClass('active-rating');
        }
    });

}

/*****************************************************
 *
 *
 *          Close the modal window with the review form
 *
 *
 * ****************************************************/

function closeRm() {

    var rmCloseBtn = $('.rm-close');

    rmCloseBtn.click(function (e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('afterClose.fb', function (e, instance, slide) {
        $('.rating-field').removeClass('active-rating');
        $('#new-review').find('form').find("input[type=text], textarea").val("");
        $('#new-review').find('form').find("input[type=radio]").prop('checked', false);
    });

}

/*****************************************************
 *
 *
 *          Show hidden text on mobile devices
 *
 *
 * ****************************************************/

function moreContent() {

    var button = $('.btn-mc'),
        content = button.closest('.hidden-text-box').find('.hidden-content');

    button.click(function () {
        if (!$(this).hasClass('isOpened')) {
            content.addClass('full-content');
            $(this).addClass('isOpened');
        } else {
            content.removeClass('full-content');
            $(this).removeClass('isOpened');
        }

        var btnCurrentText = $('span', this).text(),
            btnNewText = $(this).attr('data-text');

        $('span', this).text(btnNewText);
        $(this).attr('data-text', btnCurrentText);
    });

}